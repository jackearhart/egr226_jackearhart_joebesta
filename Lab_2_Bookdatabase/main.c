/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Jack Earhart & Joe Besta
 *Date          5/20/2020
 *Assignment    Lab 2: Book Database
 *File          Main.c
 *Description:  This program is designed to search for books from a given data
 *              file. The user is asked which method they would like to search
 *              for a book by. The options are searching by book title, author,
 *              or ISBN. There is error checking throughout the program to make
 *              sure the user enters valid input.
 *
 */

//include libraries for compiling
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//macro definitions
#define FILENAME "BookList.csv"
#define NUM_BOOKS 360
#ifndef NULL
#define NULL 0
#endif

//structure definition
typedef struct{
    char title [255];
    char author_name[50];
    char ISBN [10];
    int pages;
    int year_published;
}book;

//function prototypes
int get_input(int);
int parse_file(book[]);

int main(){
    book book_array[360];
    parse_file(book_array);

}

int parse_file (book book_array[]){
    FILE* infile = fopen ("BookList.csv", "r");
    if (infile == NULL){
        printf("no file found\n");
        return 0;
    }
    char buffer[512];
    int i = 0;
    printf("file found\n");
    while (fgets(buffer, 512, infile)){

        char * ptr = strtok (buffer, ",");
        if (strcmp(ptr, "N/A"))
            strcpy(book_array[i].title,ptr);

        ptr = strtok(NULL, ",\n");
        if (strcmp(ptr, "N/A"))
            strcpy(book_array[i].author_name,ptr);

        ptr = strtok(NULL, ",\n");
        if (strcmp(ptr, "N/A"))
            strcpy(book_array[i].ISBN,ptr);

        ptr = strtok(NULL, ",\n");
        if (strcmp(ptr, "N/A"))
             book_array[i].pages=atoi(ptr);
               else
                   book_array[i].pages = -1;

        ptr = strtok(NULL, ",\n");
        if (strcmp(ptr, "N/A"))
            book_array[i].year_published = atoi(ptr);
        else
            book_array[i].year_published = -1;
        i++;

    }

return 1;
}

int get_input(int value){
if (value>=0 && value <=2){
    printf ("value is within range");

}
else if(value<0 || value>2){

    printf("Please enter a value between 0 and 2");
}

return 0;
}
