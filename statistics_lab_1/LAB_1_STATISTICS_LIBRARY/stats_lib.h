/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Jack Earhart & Joe Besta
 *Date          5/20/2020
 *Assignment    Lab 1: Part 1 statistics library
 *File          stats_lib.c
 *Description   This .h file prototypes each function used in the main.c file.
 */
#ifndef STATS_LIB_H_
#define STATS_LIB_H_
#define MAX 10000

float maximum (float nums[], int n);
float minimum (float nums[], int n);
float mean (float nums[], int n);
float median(float nums[], int n);
float variance (float nums[], int n);
float standard_deviation (float nums[], int n);
float sort (float nums[], int n);
float ReadFile(float nums []);

#endif /* STATS_LIB_H_ */
