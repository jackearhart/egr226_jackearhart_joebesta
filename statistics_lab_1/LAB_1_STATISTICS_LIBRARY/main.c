/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Jack Earhart & Joe Besta
 *Date          5/20/2020
 *Assignment    Lab 1: Part 1 statistics library
 *File          main.c
 *              This program reads in data from a file "data.txt" using the function
 *              ReadFile (data, FileLength) where the input "data" is an array where
 *              the data from "data.txt" was stored FileLength is the amount of numbers
 *              held in the file. Once the data is stored in the program via an array,
 *              statistical analysis can then be done on the array using the following
 *              functions:
 *              maximum(data, FileLength)
 *              minimum (data, FileLength)
 *              mean (data, FileLength)
 *              median (data, FileLength)
 *              variance(data, FileLength)
 *              standard_deviation(data, FileLength)
 *              The functions listed above are prototyped in stats_lib.h and defined in stats_lib.c.
 *
 */

#include <stdio.h>
#include "stats_lib.h"

int main(){
        float data[MAX]; //declaring an array for the text file to be stored in
        int FileLength = ReadFile(data); //Length of array stored in FileLength

    printf ("Maximum value: %f\n",maximum(data, FileLength));
    printf ("Minimum value: %f\n",minimum (data, FileLength));
    printf ("Mean Value: %f\n", mean (data, FileLength));
    printf ("Median Value: %f\n",median (data, FileLength));
    printf ("Variance value: %f\n",variance(data, FileLength));
    printf ("Standard Deviation value: %f\n",standard_deviation(data, FileLength));
}
