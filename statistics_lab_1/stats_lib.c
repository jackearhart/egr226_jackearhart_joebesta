/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Jack Earhart & Joe Besta
 *Date          5/20/2020
 *Assignment    Lab 1: Part 1 statistics library
 *File          stats_lib.c
 *Description   This .c file defines each function that was prototyped in the stats_lib.h
 *              header file.
 */
#include <stdio.h>
#include <math.h>
#include "stats_lib.h"

//custom function definitions
float ReadFile (float Nums[]) //scans in data from file data.txt
{
    FILE *fpointer;
    fpointer=fopen("data.txt","r");
    int n;
   if (fpointer == NULL){
            printf("File data.txt does not exist\n");
            return(0);
}

for (n=0; fscanf(fpointer,"%f",&Nums[n]) != EOF;n++);
return n;
}

float maximum(float nums[ ], int n) //finds maximum value of a given data set
{
int j;
float max ;
 for(j=1;j<n;j++){
                if(nums[j]>=max){
                max=nums[j];
}
}
return max;
}


float minimum (float nums[ ], int n) //finds minimum value of given data set
{
    
int j;
float min;

for(j=1;j<n;j++){

        if(nums[j]<min){
    min=nums[j];
        }
        }
return min;
}


float mean (float nums[ ], int n){
    int j,count=0;
    float average=0;
    for (j=0; j<=n;j++){
        count+=nums[j];
    }
    average=count/n;
    return average;
}



float median(float nums[ ],int n){
    int i,j,temp;
    float median=0;
    //sorting array in ascending order to find the middle number in data list
    for (i = 0; i < n; ++i)
          {
              for (j = i + 1; j < n; ++j)
              {
                  if (nums[i] < nums[j])
                  {
                      temp = nums[i];
                      nums[i] = nums[j];
                      nums[j] = temp;
                      //if number of elements are even
                      if(n%2 == 0)
                                 median = (nums[(n-1)/2] + nums[n/2])/2.0;
                             // if number of elements are odd
                             else
                                 median = nums[n/2];
                  }
              }
          }

return median;
}

float variance (float nums[], int n){
    int i;
    float sum1=0;
   float Average = mean (nums, n);
   for (i = 0; i < n; i++)
       {
           sum1 = sum1 + pow((nums[i] - Average), 2);
       }
   float Variance= sum1/n;
return Variance;
}

float standard_deviation (float nums[], int n){

    float Variance = variance ( nums,  n);
  float stddev= sqrt(Variance);
    return stddev;
}
