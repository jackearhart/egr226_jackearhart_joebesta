/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Joseph Besta and Jack Earhart
 *Date          5/20/20
 *Assignment    Lab 1: CCS Setup & C-Programming Refresher
 *File          main.c
 *Description   This program prompts the user to enter a credits they want to use in a gambling game. The user then
 *               enters in a wager amount of the total credits. A die is rolled for the user and for the PC. Whichever die
 *               is higher wins the round, and the credits waged are awarded to the winner. At the end, the user is asked
 *               if they would like to play again, restarting with the previous credits. There is error checking to make
 *               sure the user does not enter a wager larger than the credits entered, and that anything entered is a valid
 *               integer.
 *               The functions  used include:
 *                  - errorCheck: Checks that the user inputs a positive integer for the credits and wagers.
 *                  - rollDice: Randomly rolls a die for the user and PC, then choses a winner.
 *                  - enterCredit: Prompts user to enter credits they want to use and makes sure it is a valid input.
 *                  - wagerInput: Prompts user to enter a wager and makes sure it is a valid input less than or equal to the credit available.
 *
*
 */



#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "dice_game.h"


int main()
{
    int credit;
    int wager;
    int newCredit;
    int repeat = 1; //sets the repeat to 1 so the program can run through the first time

    enterCredit(&credit);

    do
    {
        wagerInput(&wager, &credit);
        rollDice(&wager, &credit);

        printf("\nWould you like to play again? (1=Yes, 0=No)\n");
        scanf("%d", &repeat);

    }
    while (repeat == 1);
}

