/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Joseph Besta and Jack Earhart
 *Date          5/20/20
 *Assignment    Lab 1: CCS Setup & C-Programming Refresher
 *File          dice_game.c
 *Description   This header file prototypes all functions used in main.c and defined in dice_game.c
 */

#ifndef DICE_GAME_H_
#define DICE_GAME_H_

double errorCheck();
void rollDice(int*,int*);
void enterCredit(int*);
void wagerInput(int*,int*);

#endif /* DICE_GAME_H_ */
