/*Course        EGR 226: Microcontroller Programming and Applications
 *Author        Joseph Besta and Jack Earhart
 *Date          5/20/20
 *Assignment    Lab 1: CCS Setup & C-Programming Refresher
 *File          dice_game.c
 *Description   This source file includes all necessary function definitions used in main.c
 */

#include "dice_game.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void wagerInput(int *wager, int *credit)
{
    printf("\nPlease enter a wager: \n");
    *wager = errorCheck();

    if (*wager > *credit || *wager <= 0) //does not allow user to enter wager more than the credit or less than or equal to 0
    {
        do
        {
            printf("\nPlease enter a wager greater than 0 and less than or equal to the number of credits.\n");
            *wager = errorCheck();
        }
        while (*wager > *credit || *wager == 0); //loops through checking for valid input until user enters correct info for wager
    }
}


void enterCredit(int *credit)
{
    printf("\nHow many credits you would like to start with? (please enter a positive integer) \n");
    *credit = errorCheck();

    if (*credit <= 0)
    {
        do
        {
            printf("\nPlease enter a credit greater than 0.\n");
            *credit = errorCheck();
        }
        while (*credit <= 0); //loops through checking for a valid input until user enters correct info for credits
    }

}


void rollDice(int *wager, int *credit)
{
    int i = 0;
    int dice[100];
    srand(time(NULL)); //used to roll die

    for (i = 0; i < 2; i++) //gets 2 rolls for user and for the computer
    {
        dice[i] = (rand() % 6) + 1; //rolls a die of 6 sides randomly

        if (i == 0)
        {
            printf("\nYou rolled a ");
        }
        if (i == 1)
        {
            printf("\nPC rolled a ");
        }
        printf("%d\n", dice[i]);
    }

    if (dice[0] > dice[1])
    {
        printf("\nYou won! [%d] credits were added to your account.\n", *wager);
        *credit = (*credit + *wager);
        printf("\nYour current account balance is [%d] credits.\n", *credit);
    }
    if (dice[0] < dice[1])
    {
        printf("\nSorry, you lost. [%d] credits removed from your account\n", *wager);
        *credit = (*credit - *wager);
        printf("\nYour current account balance is [%d] credits.\n", *credit);
    }
    if (dice[0] == dice[1])
    {
        printf("\nYou tied, wager of [%d] returned to account.\n", *wager);
        printf("\nYour current account balance is [%d] credits.\n", *credit);
    }

}


double errorCheck(int input)
{
    int flag;

    do
    {
        flag = scanf("%d", &input); //Sets flag equal to 1 if valid input or 0 if not.
        if (flag == 0 || input < 0)
        {
            printf("\nError, please enter valid input.\n");
            while (getchar() != '\n');
        }
    }
    while (flag == 0 || input < 0); //Executes do statement while flag is 0.

    return input;
}




